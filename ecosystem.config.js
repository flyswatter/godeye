module.exports = {
    /**
     * Application configuration section
     * http://pm2.keymetrics.io/docs/usage/application-declaration/
     */
    apps: [
        {
            name: 'fjtx-system',
            script: 'main.js',
            //watch: ['.nuxt', 'build'],
            env: {
                COMMON_VARIABLE: 'true'
            },
            env_production: {
                NODE_ENV: 'production'
            },
            'instances': 2,
            'exec_mode': 'cluster',
            'error_file': 'err.log',
            'out_file': 'out.log',
            'merge_logs': true,
            'log_date_format': 'YYYY-MM-DD HH:mm Z',
        },
    ],
};
