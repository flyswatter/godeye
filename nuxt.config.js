let dayjs = require('dayjs');
let config = require('./config');
const pkg = require('./package');

module.exports = {
    mode: 'universal',

    /*
     ** Headers of the page
     */
    head: {
        title: pkg.name,
        titleTemplate: titleChunk => {
            // If undefined or blank then we don't need the hyphen
            return '盯盘助手 - 上帝视角';
        },
        meta: [
            {
                charset: 'utf-8'
            },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1, user-scalable=no'
            },
            {
                hid: 'description',
                name: 'description',
                content: pkg.description
            },
            {
                name: 'version',
                content: config.verison,
                time: dayjs().format('YYYY-MM-DD HH:mm:ss SSS [Z] A') // 展示
            },
            {
                'http-equiv': 'Cache-Control',
                content: 'no-cache, no-store, must-revalidate'
            },
            {
                'http-equiv': 'Pragma',
                content: 'no-cache'
            },
            {
                'http-equiv': 'Expires',
                content: '0'
            },
        ],
        link: [
            {
                rel: 'icon',
                type: 'image/x-icon',
                href: '/favicon.ico'
            }
        ]
    },

    // 路由和中间件
    router: {
        // middleware: ['auth'] // 验证登录中间件
    },

    server: {
        port: 8888, // default: 3006
        host: 'localhost', // default: localhost,
        timing: {
            total: true
        }
    },

    /*
     ** Customize the progress-bar color
     */
    loading: {
        color: '#ff7c08'
    },
    /*
     ** Global CSS
     */
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        '@/plugins/axios',
        '@/plugins/log',

        {
            src: '~/plugins/day',
            ssr: true
        }
    ],
    telemetry: false,
    /*
     ** Nuxt.js modules
     */
    modules: ['@nuxtjs/axios'],
    // 'nuxt-winston-log'

    axios: {
        // See https://github.com/nuxt-community/axios-module#options
        // credentials: true,
        baseURL: 'http://localhost:6666'
    },

    /*
     ** Build configuration
     */
    build: {
        cache: true,
        sourceMap: false,
        splitChunks:{
            layouts: false,
            pages: true,
            commons: true
        },
        ssr : true,
        transpile: [/^element-ui/],
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {}
    }
};
