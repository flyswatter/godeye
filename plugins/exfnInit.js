import Vue from 'vue';
import exfninit from 'ly-exfn/init.vue';
// import exfninit from 'ly-exfn/init.vue';

import $config from '~/config';
import dayjs from 'dayjs';
import dict from '~/config/dict.json';
// import electron from 'electron';

if(!$config.isVueInit){


    exfninit.initDictFn(dict);
    exfninit.initAll(Vue);
    dict.setDictMap();
    
    try{
        try{
            Vue.prototype.$config = $config;
            Vue.prototype.$dayjs = dayjs;
            Vue.prototype.$dict = dict;
        }catch(ex){
            console.log("exfnInit init error on prototype set: " + ex.message,ex)
        }
        
    
        Vue.prototype.getStockItem=(code,name)=>{
            return {
                c:code||"",
                n:name||"",
                p:"",
                r:"",
                watch:false,
            }
        }
        
        Vue.prototype.getColor=(cur,yes)=>{
            if(cur*1>yes*1){
                return "red";
            }else if(yes*1>cur*1){
                return "green"
            }else {
                return "";
            }
        };
        Vue.prototype.getRateColor=(rate)=>{
            if(rate && rate.replace){
                rate = rate.replace("%","");
            }
            if(rate>0){
                return "red";
            }else if(rate<0){
                return "green"
            }else {
                return "black";
            }
        };
        Vue.prototype.formatYiMoney=(money)=>{
            var m = money/100000000;
            return m.toFixed(1)*1;
        };
        Vue.prototype.formatWanMoney=(money)=>{
            var m = money/10000;
            return m.toFixed(1)*1;
        };
        Vue.prototype.getRise=(newVal,oldVal)=>{
            var rise = (newVal-oldVal)*100/oldVal;
            rise = rise.toFixed(2)*1;
            return rise;
        };
        
        Vue.prototype.registerListen=(obj,opername,cb)=>{
            if(!obj["listener-"+opername]){
                obj["listener-"+opername] = true;
                var respName = opername+'Res';
                var listener = (event, res) => {
                    try{
                        console.info(respName+' dbdata:',res);
                        if(cb) cb(res,event);
                    }catch(ex){
                        obj.$message.error("查询异常:"+ex.message)
                    }
                };
                obj.$once('hook:beforeDestroy', () => {   
                    electron.ipcRenderer.removeListener(respName, listener)
                })
                electron.ipcRenderer.on(respName, listener)
            }
        };

        Vue.directive("elDrag", {
            bind(el, binding, vnode, oldVnode) {
            //获取弹框头部（这部分可双击全屏）
            debugger;
            const dialogHeaderEl = el.querySelector(".dragcont-head");
            //弹窗
            const dragDom = el.querySelector(".dragcont");
            //给弹窗加上overflow auto；不然缩小时框内的标签可能超出dialog；
            // dragDom.style.overflow = "auto";
            //清除选择头部文字效果
            //dialogHeaderEl.onselectstart = new Function("return false");
            //头部加上可拖动cursor
            dialogHeaderEl.style.cursor = "move";
            // 获取原有属性 ie dom元素.currentStyle 火狐谷歌 window.getComputedStyle(dom元素, null);
            const sty = dragDom.currentStyle || window.getComputedStyle(dragDom, null);
            let moveDown = (e) => {
                // 鼠标按下，计算当前元素距离可视区的距离
                const disX = e.clientX - dialogHeaderEl.offsetLeft;
                const disY = e.clientY - dialogHeaderEl.offsetTop;
                // 获取到的值带px 正则匹配替换
                let styL, styT;
                // 注意在ie中 第一次获取到的值为组件自带50% 移动之后赋值为px
                if (sty.left.includes("%")) {
                styL =
                    +document.body.clientWidth * (+sty.left.replace(/\%/g, "") / 100);
                styT =
                    +document.body.clientHeight * (+sty.top.replace(/\%/g, "") / 100);
                } else {
                styL = +sty.left.replace(/\px/g, "");
                styT = +sty.top.replace(/\px/g, "");
                }
                document.onmousemove = function (e) {
                // 通过事件委托，计算移动的距离
                const l = e.clientX - disX;
                const t = e.clientY - disY;
                // 移动当前元素
                dragDom.style.left = `${l + styL}px`;
                dragDom.style.top = `${t + styT}px`;
                //将此时的位置传出去
                //binding.value({x:e.pageX,y:e.pageY})
                };
                document.onmouseup = function (e) {
                document.onmousemove = null;
                document.onmouseup = null;
                };
            };
            dialogHeaderEl.onmousedown = moveDown;
            },
        });
    }catch(ex){
        console.log("exfnInit init error : " + ex.message,ex)
    }
    
    
    
    
}


$config.isVueInit = true;
