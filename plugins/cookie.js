import Cookie from 'js-cookie';

// export default Cookie;

export default function (ctx, inject) {
    inject('cookie', Cookie);
}
