

var eventTypeList =[
    {
        value: 0,
        name : "购买"
    },
    {
        value: 1,
        name : "港通"
    },
    {
        value: 3,
        name : "手工"
    },
    {
        value: 4,
        name : "日背离"
    },
    {
        value: 5,
        name : "周背离"
    },
    {
        value: 6,
        name : "跳价"
    },
    // {
    //     value: 2,
    //     name : "交易量"
    // }
];


function getMapByText(text){
    var map = {};
    var itemrows = text.split("\n");
    var itemname;
    itemrows.map((item)=>{
        var arr = item.split(" ");
        if(arr.length==0) return;
        if(!arr[0]) return;
        if(arr.length == 1){
            itemname = arr[0];
            map[itemname] = [];
            return;
        }else{
            map[itemname].push(item);
        }
    });
    return map;
}



export default {
    eventTypeList: eventTypeList,
    getEventTypeMap: function() {
        var eventTypeMap={};
        eventTypeList.map(function(item){
            eventTypeMap[item.value]=item.name;
        })
        return eventTypeMap;
    },
}
