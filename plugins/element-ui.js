import Vue from 'vue'
import Element from 'element-ui'
import echarts from 'echarts' // 引入echarts


Vue.prototype.$echarts = echarts;

Element.Dialog.props.closeOnClickModal.default=false;


Vue.use(Element)
