import {
    conditionalExpression
} from '@babel/types';

import { Message } from 'element-ui';

export const state = () => ({
    user: null,
});

export const mutations = {
    SET_USER(state, user) {
        state.user = user;
        // state.user.token = '85Q2j9fuN621i8Ud';
        // state.user.guid = 'zzw_1';

    }
};

export const actions = {
    // store, ctx
    nuxtServerInit(store, ctx) {
        const {
            req,
            app
        } = ctx;
        const {
            commit
        } = store;

        if (req.session.user) {
            commit('SET_USER', req.session.user);
        }
    },

};