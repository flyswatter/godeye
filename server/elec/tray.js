const { app, Menu, Tray,ipcMain } = require('electron')




if(!app) return;

app.whenReady().then(()=>{
  global.tray = new Tray('./res/sun.ico')
  createMenu();
  global.tray.on("click",()=>{
    if(global.tradeWin.isVisible()){
      global.tradeWin.hide();
    }else{
      global.tradeWin.show();
    }
    createMenu();
  });
  global.tray.on("double-click",()=>{
    global.tray.destroy();
    app.quit();
    process.exit()
  });
  global.tray.setToolTip('所有的功能窗口都可以在这边进行显示/隐藏')
})

function createMenu(){
  var menulist = [];
  if(global.mainWin){
    menulist.push(getMenu("功能窗口(alt+c)",global.mainWin));
  }
  if(global.tradeWin){
    menulist.push(getMenu("实时窗口(alt+x)",global.tradeWin));
  }
  if(global.popWin){
    menulist.push(getMenu("浮动小窗口(Alt+Space)",global.popWin));
  }
  if(global.dfcfWin){
    menulist.push(getMenu("东财窗口",global.dfcfWin));
  }
  if(global.kplWin){
    menulist.push(getMenu("开盘啦窗口",global.kplWin));
  }
  if(global.tipWin){
    menulist.push(getMenu("提示窗口",global.tipWin));
  }
  menulist.push({
    label : "退出(双击图标也可以退出)",
    type : "normal",
    click(){
      global.tray.destroy();
      app.quit();
      process.exit()
    }
  });
  const contextMenu = Menu.buildFromTemplate(menulist)
  global.tray.setContextMenu(contextMenu)
}

global.createTrayMenu = createMenu;

function getMenu(name,win){
  var menuItem = {
    label : name,
    type : "radio",
    checked : win.isVisible(),
    click(){
      if(win.isVisible()){
        win.hide();
      }else{
        win.show();
      }
      createMenu();
    }
  }
  return menuItem;
}